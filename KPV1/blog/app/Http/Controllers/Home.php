<?php

namespace App\Http\Controllers;

use App\M_Siswa;
use App\Modal;
use App\Petugas;
use App\User;
use App\Rules\validasiform;
use Illuminate\Http\Request;

class Home extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function index()
    {
        return view('Dashboard');
    }

    public function formpetugas()
    {
//        return view('forms.formpegawai');
        $data_petugas = \App\Petugas::paginate(10);
        return view ('forms.formpetugas',['data_petugas' => $data_petugas]);
    }

    public function formmodal()
    {
        return view('forms.formmodal');
    }


    public function ajax_list()
    {
//        $data = array();
//        $list = Petugas::all();
//        foreach ($list as $row) {
//            $val = array();
//            $val[] = $row->id;
//            $val[] = $row->nama;
//            $val[] = $row->alamat;
//            $val[] = $row->no_telp;
//            $val[] = $row->tmpt_lahir;
//            $val[] = $row->tgl_lahir;
//            $val[] = $row->jabatan;
//            $val[] = '<div style="text-align: center;">'
//                . '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="ganti(' . "'" . $row->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>&nbsp;'
//                . '<a class="DataTables warning: table id=example - Invalid JSON response. For more information about this error, please see http://datatables.net/tn/1btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus(' . "'" . $row->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Delete</a>'
//                . '</div>';
//            $data[] = $val;
//        }
//        $output = array("data" => $data);
//        echo json_encode($output);

//        $data_petugas = \App\Petugas::paginate(10);
//        return view ('forms.formpegawai',['data_petugas' => $data_petugas]);
    }

    public function list_modal()
    {
        $data = array();
        $list = Modal::all();
        foreach ($list as $row) {
            $val = array();
            $val[] = $row->id_modal;
            $val[] = $row->jumlah_modal_awal;
            $val[] = $row->jumlah_modal_sekarang;
            $val[] = '<div style="text-align: center;">'
                . '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="ganti(' . "'" . $row->id_modal . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>&nbsp;';
            $data[] = $val;
        }
        $output = array("data" => $data);
        echo json_encode($output);
    }

    public function getmodal($id)
    {
        $data = Modal::where('id_modal', $id)->first();
        echo json_encode($data);
    }


    public function simpanpegawai(Request $request)
    {
        $messages = [
//            'nama.required' => 'Field Nama Gak Boleh Kosong',
//            //'nama.alpha' => 'Field Nama Hanya Boleh Huruf',
//            'alamat.required' => 'Field Alamat Gak Boleh Kosong',
//            'notelp.required' => 'Field no telpon Gak Boleh Kosong',
//            'notelp.min:10' => 'Field no telpon Harus minimal 10 ',
//            'tmpt_lahir.required' => 'Field tempat lahir harus diisi !',
//            'tgl_lahir.required' => 'Field Tanggal Lahir harus diisi !',
//            'tgl_lahir.date' => 'Field Tanggal Lahir harus format tanggal !',
//            'jabatan.required' => 'Field jabatan harus diisi !',
//            'jabatan.alpha' => 'Field jabatan Hanya Boleh Huruf',
        ];

        $validator = \Validator::make($request->all(), [
//            'nama' => 'required',
//            'alamat' => 'required',
//            'notelp' => 'required|min:10',
//            'tmpt_lahir' => 'required',
//            'tgl_lahir' => 'required|date',
//            'jabatan' => 'required|alpha',

        ], $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {



            $user = new \App\User;
            $user -> name = $request -> nama;
            $user -> role = $request -> jabatan;
            $user -> email = $request -> no_telp;
            $user -> password = bcrypt('rahasia');
            $user -> remember_token = str_random(60);
            $user ->save();


            $request->request->add(['user_id'=>$user->id]);
            \App\Petugas::create($request->all());
            return redirect('/formpegawai');

//            $obj->id = $req->input('kode');
//            $obj->nama = $req->input('nama');
//            $obj->alamat = $req->input('alamat');
//            $obj->no_telp = $req->input('notelp');
//            $obj->tmpt_lahir = $req->input('tmpt_lahir');
//            $obj->tgl_lahir = $req->input('tgl_lahir');
//            $obj->jabatan = $req->input('jabatan');

//            $request->request->add(['user_id'=>$user->id]);
//            \App\Petugas::create($request->all());
//            $simpan = $obj->save();
//          //  $user ->save();
//            if ($simpan == 1) {
//                $status = "Tersmpan";
//            } else {
//                $status = "Gagal";
//            }
//            echo json_encode(array("status" => $status));
        }
    }

    public function simpanmodal(Request $req)
    {

        $messages = [
            'modal_awal.required' => 'Modal Awal Gk Boleh Kosong',
            'modal_sekarang.required' => 'Modal Sekarang Gk Boleh Kosong',
            'modal_awal.numeric' => 'Modal Awal Harus Diisi Angka!',
        ];

        $validator = \Validator::make($req->all(), [
            'modal_awal' => 'required|numeric',
            'modal_sekarang' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {
            $obj = new Modal();
            $obj->id_modal = $req->input('kode');
            $obj->jumlah_modal_awal = $req->input('modal_awal');
            $obj->jumlah_modal_sekarang = $req->input('modal_sekarang');
            $simpan = $obj->save();
            if ($simpan == 1) {
                $status = "Tersmpan";
            } else {
                $status = "Gagal";
            }
            echo json_encode(array("status" => $status));
        }
//        return response()->json(['success'=>'Record is successfully added']);
    }


    public function getpegawai($id)
    {

        $data = Petugas::where('id', $id)->first();
        echo json_encode($data);

    }

    public function updatepegawai(Request $req)
    {
        $messages = [
            'nama.required' => 'Field Nama Gak Boleh Kosong',
            'nama.alpha' => 'Field Nama Hanya Boleh Huruf',
            'alamat.required' => 'Field Alamat Gak Boleh Kosong',
            'notelp.required' => 'Field no telpon Gak Boleh Kosong',
            'notelp.min:10' => 'Field no telpon Harus minimal 10 ',
            'tmpt_lahir.required' => 'Field tempat lahir harus diisi !',
            'tgl_lahir.required' => 'Field Tanggal Lahir harus diisi !',
            'tgl_lahir.date' => 'Field Tanggal Lahir harus format tanggal !',
            'jabatan.required' => 'Field jabatan harus diisi !',
            'jabatan.alpha' => 'Field jabatan Hanya Boleh Huruf',
        ];

        $validator = \Validator::make($req->all(), [
            'nama' => 'required|alpha',
            'alamat' => 'required',
            'notelp' => 'required|min:10',
            'tmpt_lahir' => 'required',
            'tgl_lahir' => 'required|date',
            'jabatan' => 'required|alpha',

        ], $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {
            $update = Petugas::where('idd', $req->input('kode'))->update([
                'nama' => $req->input('nama'),
                'alamat' => $req->input('alamat'),
                'no_telp' => $req->input('notelp'),
                'tmpt_lahir' => $req->input('tmpt_lahir'),
                'tgl_lahir' => $req->input('tgl_lahir'),
                'jabatan' => $req->input('jabatan'),
            ]);

            if ($update == 1) {
                $status = "Tersmpan";
            } else {
                $status = "Gagal";
            }
            echo json_encode(array("status" => $status));
        }
    }

    public function updatemodal(Request $req)
    {

        $update = Modal::where('id_modal', $req->input('kode'))->update([
            'jumlah_modal_awal' => $req->input('modal_awal'),
            'jumlah_modal_sekarang' => $req->input('modal_sekarang')
        ]);

        if ($update == 1) {
            $status = "Tersmpan";
        } else {
            $status = "Gagal";
        }
        echo json_encode(array("status" => $status));
    }

    public function hapuspegawai($id)
    {
        $hapus = Petugas::where('id', $id)->delete();
        if ($hapus == 1) {
            $status = "Terhapus";
        } else {
            $status = "Gagal";
        }
        echo json_encode(array("status" => $status));
    }
}
