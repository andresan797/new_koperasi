<?php

namespace App\Http\Controllers;

use App\Petugas;
use Illuminate\Http\Request;
use Carbon\Carbon;
use \App\Nasabah;

class NasabahController extends Controller
{
    public function viewnasabah(){
        $data_nasabah = \App\Nasabah::all();
        $dataku = Petugas::all();
        return view('forms.formnasabah', compact('dataku','data_nasabah'));
       // return view ('forms.formnasabah',['data_nasabah' => $data_nasabah]);
    }

    public function format()
    {
        $data = [['nama' => null,'telp'=>null ,'alamat_ktp' => null, 'alamat_sekarang' => null, 'tempat_lahir' => null, 'tgl_lahir' => null, 'jkel' => null, 'status' => null, 'nama_umkm' => null]];
        $fileName = 'foto_ktp';


        $export = Excel::create($fileName.date('Y-m-d_H-i-s'), function($excel) use($data){
            $excel->sheet('nasabah', function($sheet) use($data) {
                $sheet->fromArray($data);
            });
        });

        return $export->download('xlsx');
    }

    public function create(){
        return view('forms.formnasabah');
    }


    public function store(Request $request){

//        $petugas = new \App\User;

//        $request->request->add(['petugas_id'=>$petugas->id]);
//        \App\Nasabah::create($request->all());
//        return redirect('/nasabah');

        if($request->file('foto_ktp')) {
            $file = $request->file('foto_ktp');
            $dt = Carbon::now();
            $acak  = $file->getClientOriginalExtension();
            $fileName = rand(11111,99999).'-'.$dt->format('Y-m-d-H-i-s').'.'.$acak;
            $request->file('foto_ktp')->move("images/nasabah", $fileName);
            $foto_ktp = $fileName;
        } else {
            $foto_ktp = NULL;
        }

        Nasabah::create([
            'id_petugas' => $request->get('id_petugas'),
            'nama' => $request->get('nama'),
            'telp' => $request->get('telp'),
            'alamat_ktp' => $request->get('alamat_ktp'),
            'alamat_sekarang' => $request->get('alamat_sekarang'),
            'tempat_lahir' => $request->get('tempat_lahir'),
            'tgl_lahir' => $request->get('tgl_lahir'),
            'jkel' => $request->get('jkel'),
            'status' => $request->get('status'),
            'nama_umkm' => $request->get('nama_umkm'),
            'foto_ktp' => $foto_ktp
        ]);

        // alert()->success('Berhasil.','Data telah ditambahkan!');

        return redirect('/nasabah');
    }

    public function delete($id){
        $nasabah = \App\Nasabah::find($id);
        $nasabah -> delete($nasabah);
        return redirect('/nasabah');
    }
}
