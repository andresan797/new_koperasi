<?php

namespace App\Http\Controllers;

use App\Nasabah;
use App\Pinjaman;
use App\pinjaman_nasabah;
use Illuminate\Http\Request;

class PinjamanController extends Controller
{
    public function formpinjaman()
    {
        $dataku = Nasabah::all();
        return view('forms.formpinjaman', compact('dataku'));
    }


    public function list_pinjaman()
    {
        $data = array();
        $list = pinjaman_nasabah::all();
        foreach ($list as $row) {
            $val = array();
            $val[] = $row->id_pinjaman;
            $val[] = $row->nama;
            $val[] = $row->besar_pinjaman;
            $val[] = $row->tgl_pengajuan_pinjam;
            $val[] = $row->lama_angsuran;
            $val[] = $row->bunga;
            $val[] = $row->angsuran_perminggu;
           $val[] = $row->total_dibayar;
            $val[] = $row->status;
            $val[] = $row->tgl_acc;
            $val[] = '<div style="text-align: center;">'
                . '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="ganti(' . "'" . $row->id_pinjaman . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>'
                .'</div>';
            $data[] = $val;
        }
        $output = array("data" => $data);
        echo json_encode($output);
    }

    public function simpanpinjaman(Request $req)
    {
        $messages = [

            'besar_pinjaman.required' => 'Field besar pinjaman Gak Boleh Kosong',
            'besar_pinjaman.numeric' => 'Field besar pinjaman harus angka',
            'tgl_pengajuan.required' => 'Field tgl pengajuan harus diisi',
            'tgl_pengajuan.date' => 'Field tgl pengajuan harus tanggal',
            'angsuran_perminggu.required' => 'Field angsuran harus diisi',
            'angsuran_perminggu.numeric' => 'Field angsuran harus angka',
            'status.required' => 'Field diisi harus diisi',
            'tgl_acc.required' => 'Field tgl acc harus diisi',
            //'tgl_acc.date' => 'Field tgl acc harus format tgl ',
        ];

        $validator = \Validator::make($req->all(), [
            'besar_pinjaman' => 'required|numeric',
            'tgl_pengajuan' => 'required|date',
            'angsuran_perminggu' => 'required|numeric',
            'status' => 'required',
            'tgl_acc' => 'required',
        ], $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {
            $obj = new Pinjaman();
            $obj->id_pinjaman = $req->input('kode');
            $obj->id_anggota = $req->input('id_anggota');
            $obj->id_modal = $req->input('id_modal');
            $obj->besar_pinjaman = $req->input('besar_pinjaman');
            $obj->tgl_pengajuan_pinjam = $req->input('tgl_pengajuan');
            $obj->lama_angsuran = $req->input('lama_angsuran');
            $obj->bunga = $req->input('bunga');
            $obj->angsuran_perminggu = $req->input('angsuran_perminggu');
            $obj->total_dibayar = $req->input('total_bayar');
            $obj->status = $req->input('status');
            $obj->tgl_acc = $req->input('tgl_acc');
            $simpan = $obj->save();

            if ($simpan == 1) {
                $status = "Tersmpan";
            } else {
                $status = "Gagal";
            }
            echo json_encode(array("status" => $status));
        }
    }

    public function getpinjaman($id)
    {

        $data = Pinjaman::where('id_pinjaman', $id)->first();
        echo json_encode($data);

    }

    public function updatepinjaman(Request $req)
    {
        $messages = [

            'besar_pinjaman.required' => 'Field besar pinjaman Gak Boleh Kosong',
            'besar_pinjaman.numeric' => 'Field besar pinjaman harus angka',
            'tgl_pengajuan.required' => 'Field tgl pengajuan harus diisi',
            'tgl_pengajuan.date' => 'Field tgl pengajuan harus tanggal',
            'angsuran_perminggu.required' => 'Field angsuran harus diisi',
            'angsuran_perminggu.numeric' => 'Field angsuran harus angka',
            'status.required' => 'Field diisi harus diisi',
            'tgl_acc.required' => 'Field tgl acc harus diisi',
            'tgl_acc.date' => 'Field tgl acc harus format tgl ',
        ];

        $validator = \Validator::make($req->all(), [
            'besar_pinjaman' => 'required|numeric',
            'tgl_pengajuan' => 'required|date',
            'angsuran_perminggu' => 'required|numeric',
            'status' => 'required',
            'tgl_acc' => 'required|date',
        ], $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {
            $update = Pinjaman::where('id_pinjaman', $req->input('kode'))->update([
                'id_anggota' => $req->input('id_anggota'),
                'besar_pinjaman' => $req->input('besar_pinjaman'),
                'tgl_pengajuan_pinjam' => $req->input('tgl_pengajuan'),
                'lama_angsuran' => $req->input('lama_angsuran'),
                'angsuran_perminggu' => $req->input('angsuran_perminggu'),
                'total_dibayar' => $req->input('total_bayar'),
                'status' => $req->input('status'),
                'tgl_acc' => $req->input('tgl_acc')
            ]);

            if ($update == 1) {
                $status = "Tersmpan";
            } else {
                $status = "Gagal";
            }
            echo json_encode(array("status" => $status));
        }
    }
}
