<?php

namespace App\Http\Controllers;

use App\Angsuran;
use App\detail_pinjaman;
use App\Pinjaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AngsuranController extends Controller
{


    public function formangsur()
    {
        $dataku = Pinjaman::all();
        return view('forms.formangsur', compact('dataku'));
    }

    public function getID($id)
    {
        $data = detail_pinjaman::where('id_pinjaman', $id)->first();
        echo json_encode($data);
    }

    public function getangsuran($id)
    {
//        $data = DB::table('tb_angsuran')->select(DB::raw('max(angsuran_ke'))->where('id_pinjaman',$id);
   //$data = Angsuran::where('id_pinjaman',$id)->max('angsuran_ke')->first();
        //$data = DB::table('tb_angsuran')->max('angsuran_ke')->where('id_pinjaman',$id)->first();
        $data= Angsuran::select(DB::raw('max(angsuran_ke) as angsuran_ke'))->where('id_pinjaman', $id)->first();
        echo json_encode($data);
    }
    public function selectangsuran($id)
    {
        $data = Angsuran::where('no_trans', $id)->first();
        echo json_encode($data);
    }

    public function list_angsuran()
    {
        $data = array();
        $list = Angsuran::all();
        foreach ($list as $row) {
            $val = array();
            $val[] = $row->no_trans;
            $val[] = $row->tgl_angsur;
            $val[] = $row->id_pinjaman;
            $val[] = $row->id_nasabah;
            $val[] = $row->nama;
            $val[] = $row->angsuran_ke;
            $val[] = $row->besar_angsuran;
            $val[] = '<div style="text-align: center;">'
                . '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="ganti(' . "'" . $row->no_trans . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>'
                .'</div>';
            $data[] = $val;
        }
        $output = array("data" => $data);
        echo json_encode($output);
    }


    public function simpanangsuran(Request $req)
    {
        $messages = [
            'no_trans.numeric' => 'Field no transaksi harus angka',
            'tgl_angsur.date' => 'Field tanggal harus format tanggal',
            'tgl_angsur.required' => 'Field tanggal gak boleh kosong',
            'id_pinjaman.required' => 'Field id_pinjam harus diisi ',
            'id_nasabah.required' => 'Field id_nasabah harus diisi',
            'nama.required' => 'Field nama harus diisi',
            'angsuran_ke.required' => 'Field angsuran ke  harus diisi',
            'besar_angsuran.required' => 'Field besar angsuran harus diisi',

        ];

        $validator = \Validator::make($req->all(), [

            'tgl_angsur' => 'required|date',
            'id_pinjaman' => 'required',
            'id_nasabah' => 'required',
            'nama' => 'required',
            'angsuran_ke' => 'required',
            'besar_angsuran' => 'required',
        ], $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {
            $obj = new Angsuran();
            $obj->no_trans = $req->input('kode');
            $obj->tgl_angsur = $req->input('tgl_angsur');
            $obj->id_pinjaman = $req->input('id_pinjaman');
            $obj->id_nasabah = $req->input('id_nasabah');
            $obj->nama = $req->input('nama');
            $obj->angsuran_ke = $req->input('angsuran_ke');
            $obj->besar_angsuran = $req->input('besar_angsuran');
            $simpan = $obj->save();
            if ($simpan == 1) {
                $status = "Tersmpan";
            } else {
                $status = "Gagal";
            }
            echo json_encode(array("status" => $status));
        }
    }
    public function updateangsuran(Request $req)
    {
        $messages = [
            'no_trans.numeric' => 'Field no transaksi harus angka',
            'tgl_angsur.date' => 'Field tanggal harus format tanggal',
            'tgl_angsur.required' => 'Field tanggal gak boleh kosong',
            'id_pinjaman.required' => 'Field id_pinjam harus diisi ',
            'id_nasabah.required' => 'Field id_nasabah harus diisi',
            'nama.required' => 'Field nama harus diisi',
            'angsuran_ke.required' => 'Field angsuran ke  harus diisi',
            'besar_angsuran.required' => 'Field besar angsuran harus diisi',

        ];

        $validator = \Validator::make($req->all(), [

            'tgl_angsur' => 'required|date',
            'id_pinjaman' => 'required',
            'id_nasabah' => 'required',
            'nama' => 'required',
            'angsuran_ke' => 'required',
            'besar_angsuran' => 'required',
        ], $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {
            $update = Angsuran::where('no_trans', $req->input('kode'))->update([
                'tgl_angsur' => $req->input('tgl_angsur'),
                'id_pinjaman' => $req->input('id_pinjaman'),
                'id_nasabah' => $req->input('id_nasabah'),
                'nama' => $req->input('nama'),
                'angsuran_ke' => $req->input('angsuran_ke'),
                'besar_angsuran' => $req->input('besar_angsuran')
            ]);
            if ($update == 1) {
                $status = "Tersmpan";
            } else {
                $status = "Gagal";
            }
            echo json_encode(array("status" => $status));
        }
    }
}
