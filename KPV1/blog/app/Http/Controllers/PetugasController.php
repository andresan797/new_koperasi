<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class PetugasController extends Controller
{
    public function viewpetugas(){

        $data_petugas = \App\Petugas::paginate(10);
        return view ('forms.formpetugas',['data_petugas' => $data_petugas]);
    }

    public function create(Request $request){

        $user = new \App\User;
        $user -> name = $request -> nama;
        $user -> role = $request -> jabatan;
        $user -> email = $request -> no_telp;
        $user -> password = bcrypt('rahasia');
        $user -> remember_token = str_random(60);
        $user ->save();


        $request->request->add(['user_id'=>$user->id]);
        \App\Petugas::create($request->all());
        return redirect('/petugas');
    }

    public function delete($id){

        $petugas = \App\Petugas::find($id);
        $petugas -> delete($petugas);
        return redirect('/petugas');
    }

    public function edit($id)
    {
        $petugas = array();
        $petugas['petugas']= \App\Petugas::where('id',$id)->first();
        return view('/petugas')->with($petugas);

    }

    public function update(Request $req){

//        $petugas = \App\Petugas::where('id', $request->input('petugas'));
//        $petugas -> update([
//
//            'nama'=>$request->input('nama'),
//            'alamat'=>$request->input('alamat'),
//            'no_telp'=>$request->input('no_telp'),
//            'tmpt_lahir'=>$request->input('tpmt_lahir'),
//            'tgl_lahir'=>$request->input('tgl_lahir'),
//            'jabatan'=>$request->input('jabatan'),
//        ]);
//        return redirect('/petugas');

        \App\Petugas::where('id', $req->input('petugas'))->update([
            'nama' => $req->input('nama'),
            'alamat' => $req->input('alamat'),
            'no_telp' => $req->input('notelp'),
            'tmpt_lahir' => $req->input('tmpt_lahir'),
            'tgl_lahir' => $req->input('tgl_lahir'),
            'jabatan' => $req->input('jabatan'),
        ]);
        return redirect('/petugas');
    }



}
