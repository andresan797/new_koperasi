<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nasabah extends Model
{
    protected $table = 'tb_nasabah';
    protected $fillable = ["nama","telp","alamat_ktp","alamat_sekarang","tempat_lahir","id_petugas",
        "tgl_lahir","jkel","status","nama_umkm","foto_ktp"];
    public $timestaps =false;

}
