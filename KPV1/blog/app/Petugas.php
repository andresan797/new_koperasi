<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
    protected $table = 'tb_petugas';
    protected $fillable = ['nama', 'alamat', 'no_telp', 'tmpt_lahir', 'tgl_lahir', 'jabatan','user_id'];
    public $timestaps = false;
}
