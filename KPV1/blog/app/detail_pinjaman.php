<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detail_pinjaman extends Model
{
    protected $table = 'detail_pinjaman';
    protected $fillable = ['id_pinjaman', 'id_nasabah','nama', 'angsuran_perminggu'];
    public $timestaps = false;
}
