<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modal extends Model
{
    protected $table = 'tb_data_modal';
    protected $fillable = ['id_modal', 'jumlah_modal_awal', 'jumlah_modal_sekarang'];
    public $timestaps = false;

}
