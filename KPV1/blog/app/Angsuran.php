<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Angsuran extends Model
{
    protected $table = 'tb_angsuran';
    protected $fillable = ['no_trans', 'tgl_angsur','id_pinjaman','id_nasabah','nama','angsuran_ke', 'besar_angsuran'];
    public $timestaps = false;
}
