<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
    protected $table = 'tb_pinjaman';
    protected $fillable = ['id_pinjaman', 'id_anggota', 'id_modal', 'besar_pinjaman', 'tgl_pengajuan_pinjam', 'lama_angsuran', 'bunga', 'angsuran_perminggu','total_dibayar', 'status', 'tgl_acc'];
    public $timestaps = false;
}
