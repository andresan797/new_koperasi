<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pinjaman_nasabah extends Model
{
    protected $table = 'pinjaman_nasabah';
    protected $fillable = ['id_pinjaman', 'nama', 'besar_pinjaman', 'tgl_pengajuan_pinjam', 'lama_angsuran', 'bunga', 'angsuran_perminggu','total_dibayar'];
}
