@extends('layout.master')
@section('content')
    <script type="text/javascript">

        var save_method; //for save method string
        var table;

        $(document).ready(function () {
            table = $('#example').DataTable({
                "ajax": "<?php echo url('/datamodal') ?>"
            });
        });

        function reload() {
            table.ajax.reload(null, false); //reload datatable ajax
        }

        function reloadpage() {
            location.reload();
        }

        function add() {
            save_method = 'add';
            $('#formmodal')[0].reset(); // reset form on modals
            $('#modal_form').modal('show'); // show bootstrap modal
            $('.modal-title').text('Tambah Modal'); // Set Title to Bootstrap modal title
            $('[name="kode"]').prop("readonly", false);
        }

        function save() {
            var url = "";
            if (save_method === 'add') {
                url = "<?php echo url('/simpanmodal') ?>";
            } else {
                url = "<?php echo url('/updatemodal') ?>";
            }
            $.ajax({
                url: url,
                type: "POST",
                data: $('#formmodal').serialize(),
                dataType: "JSON",
                success: function (data) {
                    if(data.errors){
                        jQuery.each(data.errors, function(key, value){
                            jQuery('.alert-danger').show();
                            jQuery('.alert-danger').append('<p>'+value+'</p>');
                        });
                    }else{
                        alert(data.status);
                        $('#modal_form').modal('hide');
                        reload();
                    }
                },
                error: function (request, status, error) {
                    json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        $('.alert-danger').show("");
                        $('.alert-danger').append('<p>'+value+'</p>');
                    });
                }
            });
        }

        function ganti(id) {

            save_method = 'update';
            $('#formmodal')[0].reset(); // reset form on modals
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ganti Modal'); // Set title to Bootstrap modal title


            //Ajax Load data from ajax
            $.ajax({
                url: "<?php echo url('/'); ?>" + "/getmodal/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('[name="kode"]').prop("readonly", true);

                    $('[name="kode"]').val(data.id_modal);
                    $('[name="modal_awal"]').val(data.jumlah_modal_awal);
                    $('[name="modal_sekarang"]').val(data.jumlah_modal_sekarang);
                }, error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error get data');
                }
            });
        }
    </script>

    <section class="content-header">
        <h1>
            Manage Pegawai
        </h1>
        <ol class="breadcrumb">
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
            {{--<li><a href="#">Tables</a></li>--}}
            {{--<li class="active">Simple</li>--}}
        </ol>
    </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @if(auth()->user()->role=='Pimpinan')
                        <div class="box-header">
                            <button class="btn btn-success" onclick="add();"><i class="glyphicon glyphicon-plus"></i>
                                Tambah Modal
                            </button>
                            <button class="btn btn-default" onclick="reload();"><i
                                    class="glyphicon glyphicon-refresh"></i> Reload
                            </button>
                        </div>
                        @endif
                        <div class="box-body table-responsive no-padding">

                            <table id="example" class="table table-hover" style="text-align-last: center;">
                                <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>jumlah Modal Awal</th>
                                    <th>jumlah Modal Sekarang</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Kode</th>
                                    <th>jumlah Modal Awal</th>
                                    <th>jumlah Modal Sekarang</th>
                                    <th>Aksi</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    <div class="modal fade" id="modal_form" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Form Modal</h3>
                </div>
                <div class="modal-body form">
                    <form action="#" id="formmodal" class="form-horizontal">
                        <div class="alert alert-danger" style="display:none">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}"/>
                        <div class="form-body">
                            <div class="form-group">
                                {{--<label type="hidden" class="control-label col-md-3">Kode</label>--}}
                                <div class="col-md-9">
                                    <input name="kode" type="hidden" class="form-control" type="text"
                                           placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jumlah Modal Awal</label>
                                <div class="col-md-9">
                                    <input name="modal_awal" class="form-control" type="number" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jumlah Modal Sekarang</label>
                                <div class="col-md-9">
                                    <input name="modal_sekarang" class="form-control" type="number"
                                           placeholder="Alamat">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSave" onclick="save();" class="btn btn-primary">Save</button>
                    <button type="button" onclick="reloadpage();" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@stop
