
@extends('layout.master')

@section('content')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#Date').datepicker({
                format: "dd-mm-yyyy"
            });
        });
    </script>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Nasabah
            <small>KSP Bangun Jaya Mandiri</small>
        </h1>
        {{--<ol class="breadcrumb">--}}
        {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--<li><a href="#">Examples</a></li>--}}
        {{--<li class="active">Blank page</li>--}}
        {{--</ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class=" glyphicon glyphicon-plus"></i>Tambah Petugas</button>
                        <button class="btn btn-default" ><i class="glyphicon glyphicon-refresh"></i> Reload</button>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table id="example" class="table table-hover" style="text-align-last: center;">

                            <thead>
                            <tr>
                                {{--<th>Kode</th>--}}
                                <th>Nama</th>
                                <th>Telp</th>
                                <th>Alamat KTP</th>
                                <th>Alamat Sekarang</th>
                                {{--<th>Tempat Lahir</th>--}}
                                {{--<th>Tanggal Lahir</th>--}}
                                {{--<th>Jenis Kelamin</th>--}}
                                <th>Status</th>
                                <th>Nama UMKM</th>
                                <th>Foto KTP</th>
                                {{--<th>Foto KK</th>--}}
                                <th>Option</th>
                            </tr>
                            </thead>
                            @foreach($data_nasabah as $nasabah)
                                <tbody>
                                {{--<td>{{$petugas->id}}</td>--}}
                                <td>{{$nasabah->nama}}</td>
                                <td>{{$nasabah->telp}}</td>
                                <td>{{$nasabah->alamat_ktp}}</td>
                                <td>{{$nasabah->alamat_sekarang}}</td>
                                {{--<td>{{$nasabah->tempat_lahir}}</td>--}}
                                {{--<td>{{$nasabah->tgl_lahir}}</td>--}}
                                {{--<td>{{$nasabah->jkel}}</td>--}}
                                <td>{{$nasabah->status}}</td>
                                <td>{{$nasabah->nama_umkm}}</td>
                                <td>@if($nasabah->foto_ktp)
                                        <img src="{{url('images/nasabah/'. $nasabah->foto_ktp)}}" alt="image" style="width:75px; height:75px;" />
                                    @else
                                        <img src="{{url('images/nasabah/default.jpg')}}" alt="image" style="width:75px; height:75px; margin-right: 10px;" />
                                    @endif</td>
                                {{--<td>{{$nasabah->foto_kk}}</td>--}}
                                <td><a href="#" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>
                                <a href="/nasabah/{{$nasabah->id}}/delete" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
                                </td>
                                </tbody>
                            @endforeach
                            <tfoot>
                            <tr>
                                {{--<th>Kode</th>--}}
                                {{--<th>Nama</th>--}}
                                {{--<th>Alamat</th>--}}
                                {{--<th>Nomor Telepon</th>--}}
                                {{--<th>Tempat Lahir</th>--}}
                                {{--<th>Tanggal Lahir</th>--}}
                                {{--<th>jabatan</th>--}}
                                {{--<th>Option</th>--}}

                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--modal--}}
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Form Petugas</h3>
                </div>
                <div class="modal-body form">
                    <form action="{{route('nasabah.store')}}" method="post" id="form" class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="alert alert-danger" style="display:none">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />
                        <div class="form-body">
                            <div class="form-group">
                                {{--<label type="hidden" class="control-label col-md-3">Kode</label>--}}
                                <div class="col-md-9">
                                    <input name="kode"type="hidden" class="form-control" type="text" placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">ID_PETUGAS</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="id_petugas">
                                        @foreach ($dataku as $data)
                                            <option value="{{ $data->id }}">{{$data->id }}[{{ $data->nama}}]
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama</label>
                                <div class="col-md-9">
                                    <input name="nama" class="form-control" type="text" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">No Telpon</label>
                                <div class="col-md-9">
                                    <input name="telp" class="form-control" type="text" placeholder="No Telpon">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Alamat Sesuai KTP</label>
                                <div class="col-md-9">
                                    <input name="alamat_ktp" class="form-control" type="text" placeholder="Alamat">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Alamat Sekarang</label>
                                <div class="col-md-9">
                                    <input name="alamat_sekarang" class="form-control" type="text" placeholder="Alamat">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tempat Lahir</label>
                                <div class="col-md-9">
                                    <input name="tempat_lahir" class="form-control" type="text" placeholder="Tempat Lahir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tgl Lahir</label>
                                <div class="col-md-9">
                                    <input name="tgl_lahir" id="Date" class="form-control" type="text" placeholder="Tgl Lahir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jenis Kelamin</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="jkel">
                                        <option value="Pria">Pria</option>
                                        <option value="Wanita">Wanita</option>
                                    </select>
                                </div>
                            </div>
{{--                            <div class="form-group">--}}
{{--                                <label class="control-label col-md-3">Jenis Kelamin</label>--}}
{{--                                <div class="col-md-9">--}}
{{--                                    <input name="jkel" class="form-control" type="text" placeholder="jkel">--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="form-group">
                                <label class="control-label col-md-3">>status</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="status" >
                                        <option value="Menikah">Menikah</option>
                                        <option value="Belum Menikah">Belum Menikah</option>
                                    </select>
                                </div>
                            </div>
{{--                            <div class="form-group">--}}
{{--                                <label class="control-label col-md-3">status</label>--}}
{{--                                <div class="col-md-9">--}}
{{--                                    <input name="status" class="form-control" type="text" placeholder="status">--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama UMKM</label>
                                <div class="col-md-9">
                                    <input name="nama_umkm" class="form-control" type="text" placeholder="Nama UMKM">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-md-3">Foto KTP</label>
                                <div class="col-md-9">
                                <img id="img-upload" width="420" height="200">
                                <input name="foto_ktp" type="file" class="uploads form-control" id="imgInp">
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label class="control-label col-md-3">Upload Image</label>--}}
                                {{--<div class="input-group">--}}
                                    {{--<div class="col-md-9">--}}
                                    {{--<span class="input-group-btn">--}}
                                        {{--<span class="btn btn-default btn-file">--}}
                                            {{--Browse… <input type="file" id="imgInp">--}}
                                        {{--</span>--}}
                                    {{--</span>--}}

                                    {{--<input name="foto_ktp" type="text" class="uploads form-control" readonly>--}}

                                    {{--<img id='img-upload'/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="modal-footer">
                                <button type="submit"  class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
