@extends('layout.master')
@section('content')


    <script type="text/javascript">

        var save_method; //for save method string
        var table;

        $(document).ready(function() {
            table = $('#example').DataTable( {
                "ajax": "<?php echo url('/datapetugas') ?>"
            });
        });

        function reload(){
            table.ajax.reload(null,false); //reload datatable ajax
        }

        function add(){
            save_method = 'add';
            $('#form')[0].reset(); // reset form on modals
            $('#modal_form').modal('show'); // show bootstrap modal
            $('.modal-title').text('Tambah Pegawai'); // Set Title to Bootstrap modal title
            $('[name="kode"]').prop("readonly",false);
        }

        function save(){
            var url = "";
            if(save_method === 'add'){
                url = "<?php echo url('/simpanpegawai') ?>";
            }else{
                url = "<?php echo url('/updatepegawai') ?>";
            }
            $.ajax({
                url : url,
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    if(data.errors){
                        jQuery.each(data.errors, function(key, value){
                            jQuery('.alert-danger').show();
                            jQuery('.alert-danger').append('<p>'+value+'</p>');
                        });
                    }else{
                        alert(data.status);
                        $('#modal_form').modal('hide');
                        reload();
                    }
                },
                error: function (request, status, error) {
                    json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        $('.alert-danger').show("");
                        $('.alert-danger').append('<p>'+value+'</p>');
                    });
                }
            });
        }

        function ganti(id){

            save_method = 'update';
            $('#form')[0].reset(); // reset form on modals
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ganti Siswa'); // Set title to Bootstrap modal title


            //Ajax Load data from ajax
            $.ajax({
                url : "<?php echo url('/'); ?>"  + "/getpegawai/" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data){
                    $('[name="kode"]').prop("readonly",true);

                    $('[name="kode"]').val(data.id);
                    $('[name="nama"]').val(data.nama);
                    $('[name="alamat"]').val(data.alamat);
                    $('[name="notelp"]').val(data.no_telp);
                    $('[name="tmpt_lahir"]').val(data.tmpt_lahir);
                    $('[name="tgl_lahir"]').val(data.tgl_lahir);
                    $('[name="jabatan"]').val(data.jabatan);

                },error: function (jqXHR, textStatus, errorThrown){
                    alert('Error get data');
                }
            });
        }

        function reloadpage() {
            location.reload();
        }
        function hapus(id){
            if(confirm("Apakah anda yakin menghapus siswa dengan kode " + id + " ?")){
                // ajax delete data to database
                $.ajax({
                    url : "<?php echo url('/'); ?>" + "/delete/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function(data){
                        alert(data.status);
                        reload();
                    },error: function (jqXHR, textStatus, errorThrown){
                        alert('Error hapus data');
                    }
                });
            }
        }
    </script>


        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Manage Pinjaman
            </h1>
            <ol class="breadcrumb">
                {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
                {{--<li><a href="#">Tables</a></li>--}}
                {{--<li class="active">Simple</li>--}}
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <button class="btn btn-success" onclick="add();"><i class="glyphicon glyphicon-plus"></i> Tambah Pegawai</button>
                            <button class="btn btn-default" onclick="reload();"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
                        </div>
                        <div class="box-body table-responsive no-padding">

                            <table id="example" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    {{--<th>Kode</th>--}}
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Nomor Telepon</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Jabatan</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                @foreach($data_petugas as $petugas)
                                    <tbody>
                                    <td>{{$petugas->id}}</td>
                                    <td>{{$petugas->nama}}</td>
                                    <td>{{$petugas->alamat}}</td>
                                    <td>{{$petugas->no_telp}}</td>
                                    <td>{{$petugas->tmpt_lahir}}</td>
                                    <td>{{$petugas->tgl_lahir}}</td>
                                    <td>{{$petugas->jabatan}}</td>
                                    <td><button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal2"><i class="glyphicon glyphicon-pencil"></i></button>
                                        <a href="/petugas/{{$petugas->id}}/delete" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a></td>

                                    </tbody>
                                @endforeach
                                <tfoot>
                                <tr>
                                    {{--<th>Kode</th>--}}
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Nomor Telepon</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Jabatan</th>
                                    <th>Aksi</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    <div class="modal fade" id="modal_form" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Form Petugas</h3>
                </div>
                <div class="modal-body form">
                    <form action="/petugas/create" id="form" class="form-horizontal">
                        <div class="alert alert-danger" style="display:none">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />
                        <div class="form-body">
                            <div class="form-group">
                                {{--<label type="hidden" class="control-label col-md-3">Kode</label>--}}
                                <div class="col-md-9">
                                    <input name="kode"type="hidden" class="form-control" type="text" placeholder="Kode">
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label class="control-label col-md-3">Id jabatan</label>--}}
                                {{--<div class="col-md-9">--}}
                                    {{--<select class="form-control" name="id_jabatan">--}}
                                        {{--@foreach ($dataku as $data)--}}
                                            {{--<option value="{{ $data->id_nasabah }}">{{$data->id_nasabah }} [{{ $data->nama}}]</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama</label>
                                <div class="col-md-9">
                                    <input name="nama" class="form-control" type="text" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Alamat</label>
                                <div class="col-md-9">
                                    <input name="alamat" class="form-control" type="text" placeholder="Alamat">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">No Telpon</label>
                                <div class="col-md-9">
                                    <input name="notelp" class="form-control" type="text" placeholder="No Telpon">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tempat Lahir</label>
                                <div class="col-md-9">
                                    <input name="tmpt_lahir" class="form-control" type="text" placeholder="Tempat Lahir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tgl_Lahir</label>
                                <div class="col-md-9">
                                    <input name="tgl_lahir" class="form-control" type="text" placeholder="Tgl_Lahir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jabatan</label>
                                <div class="col-md-9">
                                    <input name="jabatan" class="form-control" type="text" placeholder="Jabatan">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                    <button type="button" onclick="reload();" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@stop
