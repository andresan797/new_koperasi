@extends('layout.master')
@section('content')


    <script type="text/javascript">

        var save_method; //for save method string
        var table;

        $(document).ready(function () {
            table = $('#example').DataTable({
                "ajax": "<?php echo url('/dataangsuran') ?>"
            });
            $('#Date').datepicker({
                format: "yyyy-mm-dd"
            });
            $('#id_pinjaman').on('change', function () {
                var optionText = $("#id_pinjaman option:selected").val();
                $.ajax({
                    url: "<?php echo url('/'); ?>" + "/getid/" + optionText,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $('[name="besar_angsuran"]').val(data.angsuran_perminggu);
                        $('[name="id_nasabah"]').val(data.id_nasabah);
                        $('[name="nama"]').val(data.nama);
                    },
                    error: function (request, status, error) {
                    }
                });
            });
            $('#id_pinjaman').on('change', function () {
                var optionText = $("#id_pinjaman option:selected").val();
                $.ajax({
                    url: "<?php echo url('/'); ?>" + "/getangsuran/" + optionText,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        // if (data!=null) {
                        if(data.angsuran_ke==null){
                            document.getElementById("angsuran_ke").value = 0;
                                var angka1 = parseInt(document.getElementById("angsuran_ke").value);
                                var hasil = angka1 + 1;
                                document.getElementById("angsuran_ke").value = hasil;
                        }else {
                            document.getElementById("angsuran_ke").value = 0;
                            $('[name="angsuran_ke"]').val(data.angsuran_ke);
                            var angka2 = parseInt(document.getElementById("angsuran_ke").value);
                            var hasilku = angka2 + 1;
                            document.getElementById("angsuran_ke").value = hasilku;
                        }

                        // }else{
                        //     document.getElementById("angsuran_ke").value = 0;
                        //     var angka1 = parseInt(document.getElementById("angsuran_ke").value);
                        //     var hasil = angka1 + 1;
                        //     document.getElementById("angsuran_ke").value = hasil;
                        // }
                    },
                    error: function (request, status, error) {
                    }
                });
            });
        });

        function reload() {
            table.ajax.reload(null, false); //reload datatable ajax
        }

        function add() {
            save_method = 'add';
            $('#form_angsuran')[0].reset(); // reset form on modals
            $('#modal_form').modal('show'); // show bootstrap modal
            $('.modal-title').text('Tambah Angsuran'); // Set Title to Bootstrap modal title
            $('[name="kode"]').prop("readonly", false);
        }

        function view() {
            save_method = 'add';
            $('#tablemodal').modal('show'); // show bootstrap modal
        }

        function save() {
            var url = "";
            if (save_method === 'add') {
                url = "<?php echo url('/simpanangsuran') ?>";
            } else {
                url = "<?php echo url('/updateangsuran') ?>";
            }
            $.ajax({
                url: url,
                type: "POST",
                data: $('#form_angsuran').serialize(),
                dataType: "JSON",
                success: function (data) {
                    if (data.errors) {
                        jQuery.each(data.errors, function (key, value) {
                            jQuery('.alert-danger').show();
                            jQuery('.alert-danger').append('<p>' + value + '</p>');
                        });
                    } else {
                        alert(data.status);
                        $('#modal_form').modal('hide');
                        reload();
                    }
                },
                error: function (request, status, error) {
                    alert("Error json " + error);
                    json = $.parseJSON(request.responseText);
                    $.each(json.errors, function (key, value) {
                        $('.alert-danger').show("");
                        $('.alert-danger').append('<p>' + value + '</p>');
                    });
                }
            });
        }

        function ganti(id) {

            save_method = 'update';
            $('#form_angsuran')[0].reset(); // reset form on modals
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ganti Angsuran'); // Set title to Bootstrap modal title


            //Ajax Load data from ajax
            $.ajax({
                url: "<?php echo url('/'); ?>" + "/selectangsuran/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('[name="kode"]').prop("readonly", true);
                    $('[name="kode"]').val(data.no_trans);
                    $('[name="tgl_angsur"]').val(data.tgl_angsur);
                    $('[name="id_pinjaman"]').val(data.id_pinjaman);
                    $('[name="id_nasabah"]').val(data.id_nasabah);
                    $('[name="nama"]').val(data.nama);
                    $('[name="angsuran_ke"]').val(data.angsuran_ke);
                    $('[name="besar_angsuran"]').val(data.besar_angsuran);

                }, error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error get data');
                }
            });
        }

        function reloadpage() {
            location.reload();
        }
    </script>

    <section class="content-header">
        <h1>
            Manage Angsuran
        </h1>
        <ol class="breadcrumb">
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
            {{--<li><a href="#">Tables</a></li>--}}
            {{--<li class="active">Simple</li>--}}
        </ol>
    </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <button class="btn btn-success" onclick="add();"><i class="glyphicon glyphicon-plus"></i>
                                Tambah Angsuran
                            </button>
                            <button class="btn btn-facebook" onclick="view();"><i class="glyphicon glyphicon-book"></i>
                                LIHAT DATA PINJAMAN
                            </button>
                            <button class="btn btn-default" onclick="reload();"><i
                                    class="glyphicon glyphicon-refresh"></i> Reload
                            </button>
                        </div>
                        <div class="box-body table-responsive no-padding">

                            <table id="example" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>No Transaksi</th>
                                    <th>Tanggal_Angsuran</th>
                                    <th>ID Pinjaman</th>
                                    <th>ID Nasabah</th>
                                    <th>Nama</th>
                                    <th>Angsuran Ke</th>
                                    <th>Besar Angsuran</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No Transaksi</th>
                                    <th>Tanggal_Angsuran</th>
                                    <th>ID Pinjaman</th>
                                    <th>ID Nasabah</th>
                                    <th>Nama</th>
                                    <th>Angsuran Ke</th>
                                    <th>Besar Angsuran</th>
                                    <th>Aksi</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="modal_form" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Form Petugas</h3>
                </div>
                <div class="modal-body form">
                    <form action="#" id="form_angsuran" class="form-horizontal">
                        <div class="alert alert-danger" style="display:none">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}"/>
                        <div class="form-body">
                            <div class="form-group">
                                {{--<label type="hidden" class="control-label col-md-3">Kode</label>--}}
                                <div class="col-md-9">
                                    <input name="kode" type="hidden" class="form-control" type="text"
                                           placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tanggal Angsuran </label>
                                <div class="col-md-9">
                                    <input name="tgl_angsur" id="Date" class="form-control" type="text"
                                           placeholder="Tanggal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">ID Pinjaman</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="id_pinjaman" id="id_pinjaman" onchange="">
                                        <option value="">SILAHKAN DIPILIH</option>
                                        @foreach ($dataku as $data)
                                            <option value="{{ $data->id_pinjaman }}">{{$data->id_pinjaman }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">ID Nasabah</label>
                                <div class="col-md-9">
                                    <input name="id_nasabah" class="form-control" type="text" placeholder="ID NASABAH"
                                           readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama</label>
                                <div class="col-md-9">
                                    <input name="nama" class="form-control" type="text" placeholder="Nama" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Angsuran Ke</label>
                                <div class="col-md-3">
                                    <input name="angsuran_ke" id="angsuran_ke" class="form-control" type="number"
                                           value="0" placeholder="Angsuran Ke">
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">/ 10</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Besar Angsuran</label>
                                <div class="col-md-9">
                                    <input name="besar_angsuran" class="form-control" type="number"
                                           placeholder="Besar Angsuran" readonly>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                    <button type="button" onclick="reload();" class="btn btn-danger" data-dismiss="modal">Cancel
                    </button>
                </div>\
            </div>
        </div>
    </div>

    <div class="modal fade" id="tablemodal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tabel Pinjaman</h4>
                </div>
                <div class="modal-body">
                    <<div class="container">
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>ID_Pinjaman</th>
                                <th>Besar Pinjaman</th>
                                <th>Angsuran</th>
                                <th>Status</th>
                                <th>Acc</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop
