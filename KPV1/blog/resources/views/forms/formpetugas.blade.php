@extends('layout.master')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manage Pegawai
        </h1>
        <ol class="breadcrumb">
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
            {{--<li><a href="#">Tables</a></li>--}}
            {{--<li class="active">Simple</li>--}}
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        {{--<button class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Tambah Pegawai</button>--}}
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class=" glyphicon glyphicon-plus"></i>Tambah Petugas</button>
                        <button class="btn btn-default" onclick="reload();"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
                    </div>
                    <div class="box-body table-responsive no-padding">

                        <table id="example" class="table table-hover" style="text-align-last: center;">
                            <thead>
                            <tr>
                                {{--<th>Kode</th>--}}
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Nomor Telepon</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>jabatan</th>
                                <th>Option</th>
                            </tr>
                            </thead>
                            @foreach($data_petugas as $petugas)
                            <tbody>
                                <td>{{$petugas->id}}</td>
                                <td>{{$petugas->nama}}</td>
                                <td>{{$petugas->alamat}}</td>
                                <td>{{$petugas->no_telp}}</td>
                                <td>{{$petugas->tmpt_lahir}}</td>
                                <td>{{$petugas->tgl_lahir}}</td>
                                <td>{{$petugas->jabatan}}</td>
                                <td><button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal2"><i class="glyphicon glyphicon-pencil"></i></button>
                                <a href="/petugas/{{$petugas->id}}/delete" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a></td>

                            </tbody>
                            @endforeach
                            <tfoot>
                            <tr>
                                {{--<th>Kode</th>--}}
                                {{--<th>Nama</th>--}}
                                {{--<th>Alamat</th>--}}
                                {{--<th>Nomor Telepon</th>--}}
                                {{--<th>Tempat Lahir</th>--}}
                                {{--<th>Tanggal Lahir</th>--}}
                                {{--<th>jabatan</th>--}}
                                {{--<th>Option</th>--}}

                            </tr>
                            </tfoot>
                        </table>
                        {{$data_petugas->links()}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--modal--}}
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Form Petugas</h3>
                </div>
                <div class="modal-body form">
                    <form action="/petugas/create" method="post" id="form" class="form-horizontal">
                        {{csrf_field()}}

                        <div class="alert alert-danger" style="display:none">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />
                        <div class="form-body">
                            <div class="form-group">
                                {{--<label type="hidden" class="control-label col-md-3">Kode</label>--}}
                                <div class="col-md-9">
                                    <input name="kode"type="hidden" class="form-control" type="text" placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama</label>
                                <div class="col-md-9">
                                    <input name="nama" class="form-control" type="text" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Alamat</label>
                                <div class="col-md-9">
                                    <input name="alamat" class="form-control" type="text" placeholder="Alamat">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">No Telpon</label>
                                <div class="col-md-9">
                                    <input name="no_telp" class="form-control" type="text" placeholder="No Telpon">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tempat Lahir</label>
                                <div class="col-md-9">
                                    <input name="tmpt_lahir" class="form-control" type="text" placeholder="Tempat Lahir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tgl_Lahir</label>
                                <div class="col-md-9">
                                    <input name="tgl_lahir" class="form-control" type="text" placeholder="Tgl_Lahir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">jabatan</label>
                                <div class="col-md-9">
                                    <input name="jabatan" class="form-control" type="text" placeholder="jabatan">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit"  class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    {{--moda edit--}}
    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Form Petugas</h3>
                </div>
                <div class="modal-body form">
                    <form action="/petugas/{id}/update" method="post" id="form" class="form-horizontal">
                        {{csrf_field()}}

                        <div class="alert alert-danger" style="display:none">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />
                        <div class="form-body">
                            <div class="form-group">
                                {{--<label type="hidden" class="control-label col-md-3">Kode</label>--}}
                                <div class="col-md-9">
                                    <input name="kode"type="hidden" class="form-control" type="text" placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama</label>
                                <div class="col-md-9">
                                    <input name="nama" value="{{$petugas->nama}}" class="form-control" type="text" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Alamat</label>
                                <div class="col-md-9">
                                    <input name="alamat" value="{{$petugas->alamat}}" class="form-control" type="text" placeholder="Alamat">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">No Telpon</label>
                                <div class="col-md-9">
                                    <input name="no_telp" value="{{$petugas->no_telp}}" class="form-control" type="text" placeholder="No Telpon">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tempat Lahir</label>
                                <div class="col-md-9">
                                    <input name="tmpt_lahir" value="{{$petugas->tmpt_lahir}}" class="form-control" type="text" placeholder="Tempat Lahir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tgl_Lahir</label>
                                <div class="col-md-9">
                                    <input name="tgl_lahir" value="{{$petugas->tgl_lahir}}" class="form-control" type="text" placeholder="Tgl_Lahir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">jabatan</label>
                                <div class="col-md-9">
                                    <input name="jabatan" value="{{$petugas->jabatan}}" class="form-control" type="text" placeholder="jabatan">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit"  class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
