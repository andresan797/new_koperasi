@extends('layout.master')
@section('content')


    <script type="text/javascript">

        var save_method; //for save method string
        var table;

        $(document).ready(function () {
            table = $('#example').DataTable({
                "ajax": "<?php echo url('/datapinjaman') ?>"
            });
            $('#Date').datepicker({
                format: "dd-mm-yyyy"
            });
            $('#pengajuan').datepicker({
                format: "dd-mm-yyyy"
            });
            $('#tgl_acc').datepicker({
                format: "dd-mm-yyyy"
            });
        });

        function reload() {
            table.ajax.reload(null, false); //reload datatable ajax
        }

        function add() {
            save_method = 'add';
            $('#formpinjaman')[0].reset(); // reset form on modals
            $('#modal_form').modal('show'); // show bootstrap modal
            $('.modal-title').text('Tambah Pinjaman'); // Set Title to Bootstrap modal title
            $('[name="kode"]').prop("readonly", false);
        }

        function save() {
            var url = "";
            if (save_method === 'add') {
                url = "<?php echo url('/simpanpinjaman') ?>";
            } else {
                url = "<?php echo url('/updatepinjaman') ?>";
            }
            $.ajax({
                url: url,
                type: "POST",
                data: $('#formpinjaman').serialize(),
                dataType: "JSON",
                success: function (data) {
                    if (data.errors) {
                        jQuery.each(data.errors, function (key, value) {
                            jQuery('.alert-danger').show();
                            jQuery('.alert-danger').append('<p>' + value + '</p>');
                        });
                    } else {
                        alert(data.status);
                        $('#modal_form').modal('hide');
                        reload();
                    }
                },
                error: function (request, status, error) {
                    json = $.parseJSON(request.responseText);
                    $.each(json.errors, function (key, value) {
                        $('.alert-danger').show("");
                        $('.alert-danger').append('<p>' + value + '</p>');
                    });
                }
            });
        }

        function ganti(id) {

            save_method = 'update';
            $('#formpinjaman')[0].reset(); // reset form on modals
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Pinjaman'); // Set title to Bootstrap modal title

            //Ajax Load data from ajax
            $.ajax({
                url: "<?php echo url('/'); ?>" + "/getpinjaman/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('[name="kode"]').prop("readonly", true);
                    $('[name="kode"]').val(data.id_pinjaman);
                    $('[name="id_anggota"]').val(data.id_anggota);
                    $('[name="id_modal"]').val(data.id_modal);
                    $('[name="besar_pinjaman"]').val(data.besar_pinjaman);
                    $('[name="tgl_pengajuan"]').val(data.tgl_pengajuan_pinjam);
                    $('[name="lama_angsuran"]').val(data.lama_angsuran);
                    $('[name="bunga"]').val(data.bunga);
                    $('[name="angsuran_perminggu"]').val(data.angsuran_perminggu);
                    $('[name="status"]').val(data.status);
                    $('[name="acc"]').val(data.acc);
                    $('[name="tgl_acc"]').val(data.tgl_acc);
                }, error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error get data');
                }
            });
        }

        function getvalue() {
        }

        function reloadpage() {
            location.reload();
        }

        function totalangsuran() {
            var angka1 = parseFloat(document.getElementById("besar_pinjam").value);
            var angka2 = parseInt(document.getElementById("lama_angsuran").value);
            var angka3 = parseFloat(document.getElementById("bunga").value);
            var cicilan_pokok = angka1 / angka2;
            var bunga = (angka1 * angka3) / angka2;
            var total = cicilan_pokok + bunga;
            var total_bayar = total * angka2;
            document.getElementById("angsuran_perminggu").value = total;
            document.getElementById("total_bayar").value = total_bayar;
        }
    </script>

    <section class="content-header">
        <h1>
            Manage Pinjaman
        </h1>
        <ol class="breadcrumb">
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
            {{--<li><a href="#">Tables</a></li>--}}
            {{--<li class="active">Simple</li>--}}
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xl-12">
                <div class="box">
                    @if(auth()->user()->role=='Kasir')
                        <div class="box-header">
                            <button class="btn btn-success" onclick="add();"><i class="glyphicon glyphicon-plus"></i>
                                Tambah Pinjaman
                            </button>
                            <button class="btn btn-default" onclick="reload();"><i
                                        class="glyphicon glyphicon-refresh"></i> Reload
                            </button>
                        </div>
                    @endif
                    <div class="box-body table-responsive table-bordered table-hover">
                        <table id="example" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th>ID_Pinjaman</th>
                                <th>NAMA NASABAH</th>
                                <th>Besar Pinjaman</th>
                                <th>Tanggal Pengajuan</th>
                                <th>Lama Angsuran</th>
                                <th>Bunga</th>
                                <th>Angsuran Perminggu</th>
                                <th>total_dibayar</th>
                                <th>Status</th>
                                <th>Acc</th>
                                <th>aksi</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID_Pinjaman</th>
                                <th>NAMA_NASABAH</th>
                                <th>Besar Pinjaman</th>
                                <th>Tgl Pengajuan</th>
                                <th>Lama Angsuran</th>
                                <th>Bunga</th>
                                <th>Angsuran</th>
                                <th>total_dibayar</th>
                                <th>Status</th>
                                <th>Acc</th>
                                <th>aksi</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>

    <div class="modal fade" id="modal_form" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Form Pinjaman</h3>
                </div>
                <div class="modal-body form">
                    <form action="#" id="formpinjaman" class="form-horizontal">
                        <div class="alert alert-danger" style="display:none">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}"/>
                        <div class="form-body">
                            <div class="form-group">
                                {{--<label type="hidden" class="control-label col-md-3">Kode</label>--}}
                                <div class="col-md-9">
                                    <input name="kode" type="hidden" class="form-control" type="text"
                                           placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Id Anggota</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="id_anggota">
                                        @foreach ($dataku as $data)
                                            <option value="{{ $data->id_nasabah }}">{{$data->id_nasabah }}[{{ $data->nama}}]
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
{{--                                <label class="control-label col-md-3">ID MODAL</label>--}}
                                <div class="col-md-9">
                                    <input name="id_modal" class="form-control" type="hidden" placeholder="Id Modal"
                                           value="1" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Besar Pinjaman</label>
                                <div class="col-md-9">
                                    <select class="form-control" onchange="totalangsuran()" id="besar_pinjam" name="besar_pinjaman">
                                        <option>SILAHKAN DIPILIH</option>
                                        <option value="1000000">1000000</option>
                                        <option value="1500000">1500000</option>
                                        <option value="2000000">2000000</option>
                                    </select>
                                </div>
                            </div>
{{--                            <div class="form-group">--}}
{{--                                <label class="control-label col-md-3">Besar Pinjaman</label>--}}
{{--                                <div class="col-md-9">--}}
{{--                                    <input name="besar_pinjaman" onkeyup="totalangsuran()" id="besar_pinjam"--}}
{{--                                           class="form-control" type="number" placeholder="besar_pinjaman">--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="form-group">
                                <label class="control-label col-md-3">Tgl Pengajuan</label>
                                <div class="col-md-9">
                                    <input name="tgl_pengajuan" id="Date" class="form-control" type="text"
                                           placeholder="Tanggal Pengajuan">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Lama Angsuran</label>
                                <div class="col-md-3">
                                    <input name="lama_angsuran" id="lama_angsuran" class="form-control" type="text"
                                           value="10" placeholder="Lama Angsuran" readonly>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">minggu</label>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Bunga</label>
                                <div class="col-md-9">
                                    <input name="bunga" id="bunga" class="form-control" type="text" placeholder="Bunga"
                                           value="0.3" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Angsuran Perminggu</label>
                                <div class="col-md-9">
                                    <input name="angsuran_perminggu" id="angsuran_perminggu" class="form-control"
                                           type="text" placeholder="Angsuran Perminggu">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">TOTAL BAYAR</label>
                                <div class="col-md-9">
                                    <input name="total_bayar" id="total_bayar" class="form-control" type="text"
                                           placeholder="TOTAL BAYAR">
                                </div>
                            </div>
                                <div class="form-group">
{{--                                    <label class="control-label col-md-3">Status</label>--}}
                                    <div class="col-md-9">
                                        <input name="status" class="form-control" value="NULL" type="hidden" placeholder="Status">
                                    </div>
                                </div>
                                <div class="form-group">
{{--                                    <label class="control-label col-md-3">Tgl Acc</label>--}}
                                    <div class="col-md-9">
                                        <input name="tgl_acc" id="tgl_acc" class="form-control"  value="NULL" type="hidden" type="text" placeholder="tgl_acc">
                                    </div>
                                </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                    <button type="button" onclick="reload()" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@stop
