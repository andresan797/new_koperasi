<!DOCTYPE html>
<html>
<head>
    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        #img-upload{
            width: 100%;
        }
    </style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KSP BJM</title>
    {{--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">--}}
    {{--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>--}}
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/adminlte/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/adminlte/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="{{ URL::asset('datatables/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('datatables/css/dataTables.bootstrap.css') }}">

    {{--table--}}


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>K</b>SP</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Koperasi</b>KSP</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->

                    <!-- Notifications: style can be found in dropdown.less -->

                    <!-- Tasks: style can be found in dropdown.less -->

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/adminlte/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{auth()->user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="/adminlte/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    {{auth()->user()->name}}
                                    <small>{{auth()->user()->role}}</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            {{--<li class="user-body">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-xs-4 text-center">--}}
                                        {{--<a href="#">Followers</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-xs-4 text-center">--}}
                                        {{--<a href="#">Sales</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-xs-4 text-center">--}}
                                        {{--<a href="#">Friends</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<!-- /.row -->--}}
                            {{--</li>--}}
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                {{--<div class="pull-left">--}}
                                    {{--<a href="#" class="btn btn-default btn-flat">Profile</a>--}}
                                {{--</div>--}}
                                <div class="pull-right">
                                    <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/adminlte/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{auth()->user()->name}}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            {{--<form action="#" method="get" class="sidebar-form">--}}
                {{--<div class="input-group">--}}
                    {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
                    {{--<span class="input-group-btn">--}}
                {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
                {{--</button>--}}
              {{--</span>--}}
                {{--</div>--}}
            {{--</form>--}}
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-edit"></i> <span>Forms</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>

                    <ul class="treeview-menu">
                        @if(auth()->user()->role=='Pimpinan')
                        <li><a  href="/formpegawai"><i class="fa fa-circle-o"></i> Petugas</a></li>
                        @endif

                            @if(auth()->user()->role=='Pengawas')
                                <li><a  href="/formpegawai"><i class="fa fa-circle-o"></i> Petugas</a></li>
                            @endif

                            @if(auth()->user()->role=='Kasir')
                                <li><a  href="/formmodal"><i class="fa fa-circle-o"></i> Modal</a></li>
                            @endif

                            @if(auth()->user()->role=='Mantri')
                                <li><a  href="/nasabah"><i class="fa fa-circle-o"></i> Nasabah</a></li>
                                @endif
                    </ul>

                </li>



                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Transaksi</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(auth()->user()->role=='Kasir')
                        <li><a href="/formpinjaman"><i class="fa fa-circle-o"></i> Pinjaman</a></li>
                        <li><a href="/formangsuran"><i class="fa fa-circle-o"></i> Angsuran</a></li>
                        @endif

                            @if(auth()->user()->role=='Mantri')
                                <li><a href="/formangsuran"><i class="fa fa-circle-o"></i> Angsuran</a></li>
                            @endif

                            @if(auth()->user()->role=='Pimpinan')
                                <li><a href="/formpinjaman"><i class="fa fa-circle-o"></i> Pinjaman</a></li>
                            @endif

                            @if(auth()->user()->role=='Pengawas')
                                <li><a href="/formpinjaman"><i class="fa fa-circle-o"></i> Pinjaman</a></li>
                                @endif

                    </ul>
                </li>



                <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i> <span>Dokumen</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>


                <ul class="treeview-menu">
                        @if(auth()->user()->role=='Mantri')
                        <li><a href="/Detail_Angsuran"><i class="fa fa-circle-o"></i>Detail Angsuran Perminggu</a></li>
                        @endif
                            @if(auth()->user()->role=='Kasir')
                                <li><a href="/Detail_Angsuran"><i class="fa fa-circle-o"></i>Detail Angsuran Perminggu</a></li>
                            @endif

                            @if(auth()->user()->role=='Pimpinan')
                                <li><a href="/Detail_Angsuran"><i class="fa fa-circle-o"></i>Detail Laporan Keuangan</a></li>
                            @endif

                            @if(auth()->user()->role=='Kasir')
                                <li><a href="/Detail_Angsuran"><i class="fa fa-circle-o"></i>Detail Konfirmasi Pinjaman</a></li>
                            @endif

                </ul>
                </li>



            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
        reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

            {{--<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>--}}
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
                <form action="{{route('changePassword')}}" method="post" >
                    {{csrf_field()}}
                    <h3 class="control-sidebar-heading">Atur Otentikasi Pengguna</h3>
                    <ul class="control-sidebar-menu">

                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-user bg-blue"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Username</h4>

                                <p>{{auth()->user()->email}}</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-file-code-o bg-red"></i>

                            {{--<div class="menu-info">--}}
                                {{--<h4 class="control-sidebar-subheading">Password</h4>--}}

                                {{--<p>{{auth()->user()->password}}</p>--}}
                            {{--</div>--}}
                        </a>
                    </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->


                    <h3 class="control-sidebar-heading">Ganti Password</h3>
                    <ul class="control-sidebar-menu">

                    <li>
                        <div class="form-group">
                            <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                <label for="new-password" class="col-md-12 control-label">Current Password</label>
                                    <div class="col-md-12">
                                        <input id="current-password" name="current-password"  class="form-control" type="text" placeholder="password lama">
                                        @if ($errors->has('current-password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('current-password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                            </div>

                            <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                <label for="new-password" class="col-md-12 control-label">New Password</label>
                                    <div class="col-md-12">
                                        <input id="new-password" name="new-password"  class="form-control" type="text" placeholder="password baru">
                                        @if ($errors->has('new-password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('new-password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                            </div>
                            <div class="form-group">
                                <label for="new-password-confirm" class="col-md-12 control-label">Confirm New Password</label>
                                    <div class="col-md-12">
                                        <input id="new-password-confirm" name="new-password-confirm"  class="form-control" type="text" placeholder="Confirm password baru">
                                    </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12" style="text-align-last: right">
                            <button type="submit"  class="btn btn-primary">UBAH</button>
                            </div>
                        </div>
                    </li>
                    <li>

                    </li>
                    <li>

                    </li>
                    </ul>
                </form>
                <!-- /.control-sidebar-menu -->
                    {{--<form id="form-change-password" role="form" method="POST" action="{{'/admin' }}" novalidate class="form-horizontal">--}}
                        {{--<div class="col-md-9">--}}
                            {{--<label for="current-password" class="col-sm-4 control-label">Current Password</label>--}}
                            {{--<div class="col-sm-8">--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                    {{--<input type="text"  class="form-control" id="current-password" name="current-password" placeholder="Password">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<label for="password" class="col-sm-4 control-label">New Password</label>--}}
                            {{--<div class="col-sm-8">--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="password" class="form-control" id="password" name="password" placeholder="Password">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<label for="password_confirmation" class="col-sm-4 control-label">Re-enter Password</label>--}}
                            {{--<div class="col-sm-8">--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-sm-offset-5 col-sm-6">--}}
                                {{--<button type="submit" class="btn btn-danger">Submit</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}


            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            {{--<div class="tab-pane" id="control-sidebar-settings-tab">--}}
                {{--<form method="post">--}}
                    {{--<h3 class="control-sidebar-heading">General Settings</h3>--}}

                    {{--<div class="form-group">--}}
                        {{--<label class="control-sidebar-subheading">--}}
                            {{--Report panel usage--}}
                            {{--<input type="checkbox" class="pull-right" checked>--}}
                        {{--</label>--}}

                        {{--<p>--}}
                            {{--Some information about this general settings option--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<!-- /.form-group -->--}}

                    {{--<div class="form-group">--}}
                        {{--<label class="control-sidebar-subheading">--}}
                            {{--Allow mail redirect--}}
                            {{--<input type="checkbox" class="pull-right" checked>--}}
                        {{--</label>--}}

                        {{--<p>--}}
                            {{--Other sets of options are available--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<!-- /.form-group -->--}}

                    {{--<div class="form-group">--}}
                        {{--<label class="control-sidebar-subheading">--}}
                            {{--Expose author name in posts--}}
                            {{--<input type="checkbox" class="pull-right" checked>--}}
                        {{--</label>--}}

                        {{--<p>--}}
                            {{--Allow the user to show his name in blog posts--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<!-- /.form-group -->--}}

                    {{--<h3 class="control-sidebar-heading">Chat Settings</h3>--}}

                    {{--<div class="form-group">--}}
                        {{--<label class="control-sidebar-subheading">--}}
                            {{--Show me as online--}}
                            {{--<input type="checkbox" class="pull-right" checked>--}}
                        {{--</label>--}}
                    {{--</div>--}}
                    {{--<!-- /.form-group -->--}}

                    {{--<div class="form-group">--}}
                        {{--<label class="control-sidebar-subheading">--}}
                            {{--Turn off notifications--}}
                            {{--<input type="checkbox" class="pull-right">--}}
                        {{--</label>--}}
                    {{--</div>--}}
                    {{--<!-- /.form-group -->--}}

                    {{--<div class="form-group">--}}
                        {{--<label class="control-sidebar-subheading">--}}
                            {{--Delete chat history--}}
                            {{--<a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>--}}
                        {{--</label>--}}
                    {{--</div>--}}
                    {{--<!-- /.form-group -->--}}
                {{--</form>--}}
            {{--</div>--}}
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/adminlte/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/adminlte/js/demo.js"></script>

<script>
    $(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })
</script>


{{--<script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ URL::asset('js/fnReloadAjax.js') }}"></script>--}}
{{--<script type="text/javascript"  src="{{ URL::asset('datatables/js/dataTables.bootstrap.js') }}"></script>--}}

{{--<script src="{{asset('vendors/js/vendor.bundle.base.js')}}"></script>--}}
{{--<script src="{{asset('vendors/js/vendor.bundle.addons.js')}}"></script>--}}
{{--<script src="{{asset('js/off-canvas.js')}}"></script>--}}
{{--<script src="{{asset('js/misc.js')}}"></script>--}}
{{--<script src="{{asset('js/dashboard.js')}}"></script>--}}
{{--<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>--}}
{{--<script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>--}}
{{--<script src="{{asset('js/sweetalert2.all.js')}}"></script>--}}
{{--<script src="{{asset('js/select2.min.js')}}"></script>--}}
<script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/fnReloadAjax.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('datatables/js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
</body>
</html>
