<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auths.login');
});



Route::get('/login','AdminController@login')->name('login');
Route::post('/postlogin','AdminController@postlogin');
Route::get('/logout','AdminController@logout');


Route::group(['middleware'=>['auth','checkRole:Kasir,Pimpinan,Pengawas,Mantri']],function (){

//    Route::get('/petugas','PetugasController@viewpetugas');
//    Route::post('/petugas/create','PetugasController@create');
//    Route::get('/petugas/{id}/delete','PetugasController@delete');
//    Route::post('/petugas/{id}/update','PetugasController@update');

    Route::get('/formpegawai', 'Home@formpetugas');
    Route::post('/simpanpegawai', 'Home@simpanpegawai');
    Route::get('/getpegawai/{id}', 'Home@getpegawai');
    Route::post('/updatepegawai', 'Home@updatepegawai');
    Route::get('/delete/{id}', 'Home@hapuspegawai');
    Route::get('/datapetugas','Home@ajax_list');
    Route::post('/petugas/create','Home@simpanpegawai');

    Route::resource('nasabah','NasabahController');
    Route::get('/nasabah','NasabahController@viewnasabah');
    Route::get('/nasabah/{id}/delete','NasabahController@delete');

    Route::get('/formmodal', 'Home@formmodal');
    Route::get('/datamodal', 'Home@list_modal');
    Route::post('/simpanmodal', 'Home@simpanmodal');
    Route::get('/getmodal/{id}', 'Home@getmodal');
    Route::post('/updatemodal', 'Home@updatemodal');

    Route::get('/formangsuran', 'AngsuranController@formangsur');
    Route::get('/dataangsuran', 'AngsuranController@list_angsuran');
    Route::post('/simpanangsuran', 'AngsuranController@simpanangsuran');
    Route::get('/getid/{id}', 'AngsuranController@getID');
    Route::get('/getangsuran/{id}', 'AngsuranController@getangsuran');
    Route::get('/selectangsuran/{id}', 'AngsuranController@selectangsuran');
    Route::post('/updateangsuran', 'AngsuranController@updateangsuran');

    Route::get('/formpinjaman', 'PinjamanController@formpinjaman');
    Route::get('/datapinjaman', 'PinjamanController@list_pinjaman');
    Route::post('/simpanpinjaman', 'PinjamanController@simpanpinjaman');
    Route::get('/getpinjaman/{id}', 'PinjamanController@getpinjaman');
    Route::post('/updatepinjaman', 'PinjamanController@updatepinjaman');



    Route::post('/admin','AdminController@admin');

});

Route::group(['middleware'=>['auth','checkRole:Kasir,Mantri,Pengawas,Pimpinan']],function (){

    Route::get('/dashboard','AdminController@dashboard');
    Route::post('/changePassword','AdminController@changePassword')->name('changePassword');

});

